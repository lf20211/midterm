package com.chanatip;

public class HeroPhysical {
    private String name;
    private String tribe;
    private String stat;
    private char symbol;

    public HeroPhysical(String name, String tribe, String stat, char symbol){
        this.name = name;
        this.tribe = tribe;
        this.stat = stat;
        this.symbol = symbol;
    }
    public void printName(){
        System.out.println("Name: " + name + " Tribe: " + tribe + " Stat: " + stat + " Symbol: " + symbol);
    }

    public String getName() {
        return name;
    }

    public String getTribe() {
        return tribe;
    }

    public String getStat() {
        return stat;
    }

    public char getSymbol() {
        return symbol;
    }
}
