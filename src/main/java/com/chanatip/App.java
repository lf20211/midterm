package com.chanatip;

public class App {
    public static void main(String[] args) {
        
        HeroPhysical Hoodwink = new HeroPhysical("Hoodwink,", "Animal," ,"AGI,", 'H');
        HeroPhysical DragonKnight = new HeroPhysical("Dragon Knight,", "Human," , "STR,", 'D');
        HeroPhysical DrowRanger = new HeroPhysical("Drow Ranger,", "Human," , "AGI,", 'R');
        
        HeroMagical Invoker = new HeroMagical("Invoker,", "human," , "INT,", 'I');
        HeroMagical StormSpirit = new HeroMagical("Storm spirit,",  "human," , "INT,", 'S');
        HeroMagical VoidSpirit = new HeroMagical("Void spirit,", "human," , "INT,", 'V');
        
        System.out.println("            ");
        System.out.println("            ");
        System.out.println("            ");
        System.out.println("Dota2 Hero");

        System.out.println("            ");
        System.out.println("HeroPhysical");
        System.out.println("-----------");
        Hoodwink.printName();      
        DragonKnight.printName();  
        DrowRanger.printName();   
        System.out.println("-----------");
        System.out.println("HeroMagical");
        System.out.println("-----------");
        Invoker.printName();      
        StormSpirit.printName();     
        VoidSpirit.printName();

    }

}
